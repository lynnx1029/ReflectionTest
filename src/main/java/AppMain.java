import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import servlet.Servlet;


public class AppMain {
    private static Map<String, Servlet> servletInstances = new HashMap<String, Servlet>();
    
    private static List<Class<?>> getClasses(String packageName) throws ClassNotFoundException, IOException {
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        assert classLoader != null;
        
        String path = packageName.replace('.', '/');
        Enumeration<URL> resources = classLoader.getResources(path);
        List<File> dirs = new ArrayList<File>();
        while (resources.hasMoreElements()) {
            URL resource = resources.nextElement();
            dirs.add(new File(resource.getFile()));
        }
        
        List<Class<?>> classes = new ArrayList<Class<?>>();
        for (File directory : dirs) {
            classes.addAll(findClasses(directory, packageName));
        }
        return classes;
    }
    
    private static List<Class<?>> findClasses(File directory, String packageName) throws ClassNotFoundException {
        List<Class<?>> classes = new ArrayList<Class<?>>();
        
        if (!directory.exists()) {
            return classes;
        }
        File[] files = directory.listFiles();
        for (File file : files) {
            if (file.isDirectory()) {
                assert !file.getName().contains(".");
                classes.addAll(findClasses(file, packageName + "." + file.getName()));
            } else if (file.getName().endsWith(".class")) {
                classes.add(Class.forName(packageName + '.' + file.getName().substring(0, file.getName().length() - 6)));
            }
        }
        return classes;
    }

    public static void main(String[] args) 
            throws ClassNotFoundException, IOException, InstantiationException, 
                   IllegalAccessException {
        List<Class<?>> classes = getClasses("servlet");
        
        for (Class<?> clazz: classes) {
            if (!clazz.isInterface() && Servlet.class.isAssignableFrom(clazz)) {
                servletInstances.put(clazz.getName(), (Servlet) clazz.newInstance());
            }
        }
        
        Iterator<Entry<String, Servlet>> iter = servletInstances.entrySet().iterator();
        while (iter.hasNext()) {
            Entry<String, Servlet> entry = iter.next();
            
            System.out.println("Call package name: " + entry.getKey());
            entry.getValue().doGet();
        }
    }

}
